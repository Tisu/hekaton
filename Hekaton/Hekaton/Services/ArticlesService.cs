﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using Hekaton.Models;
using Newtonsoft.Json;

namespace Hekaton.Services
{
    public class ArticlesService
    {

        public ArticlesModel GetArticles()
        {
            var deserializated = GetArticlesJson();
            var list= deserializated.results.Select(
                x => new SingleArticleModel() {Name = x.assetName, ArticleUrl = x.urlAdr});
                
                
            return new ArticlesModel() {ArticlesList = list.ToList()};
           
        }

        public RootObject GetArticlesJson()
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders
                .Accept
                .Add(new MediaTypeWithQualityHeaderValue("application/json"));
            ServicePointManager.ServerCertificateValidationCallback = (sender, cert, chain, sslPolicyErrors) => true;
            HttpWebRequest request = this.GetRequest("http://api.amp.active.com/v2/search/?near=run&current_page=1&per_page=10&sort=distance&exclude_children=true&api_key=g3h2rtp9zuyr8ws5t98yjbt6");
            WebResponse webResponse = request.GetResponse();
            var responseText = new StreamReader(webResponse.GetResponseStream()).ReadToEnd();
            var deserializated = JsonConvert.DeserializeObject<RootObject>(responseText);
            
            return deserializated;
        }

        private HttpWebRequest GetRequest(string url, string httpMethod = "GET", bool allowAutoRedirect = true)
        {
            Uri uri = new Uri(url);
            HttpWebRequest request = HttpWebRequest.Create(url) as HttpWebRequest;
            request.UserAgent = @"Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko";

            request.Timeout = Convert.ToInt32(new TimeSpan(0, 5, 0).TotalMilliseconds);
            request.Method = httpMethod;
            return request;
        }


    }
}