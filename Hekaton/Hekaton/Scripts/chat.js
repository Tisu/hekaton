﻿(function () {
    $(document).ready(function () {
        $('#close-button').on('click',
            function (event) {
                $('#chat-window').toggle('hide');
            });
    });
})();

$(document).ready(function () {
    $('#open-chat').on('click', function (event) {
        $('#chat-window').toggle('show');
    });
    $('#SendButton').on('click',
            function (event) {
                var input = $('#UserInput');
                var message = input[0].value;
                $.connection.chat.server.sendMessage(message);
                userMessage(message);

                input[0].value = '';
            });

    $('#UserInput').keyup(function(event) {
    if(event.keyCode == 13) {
        $('#SendButton').click();
    }
    });
    $.connection.hub.start()
                    .then(function () {
                        $.connection.chat.server.onChatInitialize();
                    });
    $.connection.chat.client.sendToClient = function (message, date) {
        adminMessage(message, date);
    }
    $.connection.chat.client.initChat = function (messages, userName) {
        messages.forEach(function (iterator) {
            if (iterator.User === userName) {
                userMessage(iterator.Message);
            } else {
                adminMessage(iterator.Message);
            }
        });
    };
});
var userMessage = function (message) {
    $("#conversaton-container").append(("<div class='direct-chat-msg right'><div class='direct-chat-info clearfix'><span class='direct-chat-name pull-right'>Trainer</span><span class='direct-chat-timestamp pull-left'>" + new Date().toLocaleString('en-US', { hour12: false, hour: "numeric", minute: "numeric" }) + "</span></div><img class='direct-chat-img' src='http://www.ironfieldsfit.com/wp-content/uploads/2014/08/Bench-LargeIcon.png' alt='message user image'><div class='direct-chat-text'>" + message + "</div></div>"));
};
var adminMessage = function (message, date) {

    $("#conversaton-container").append(("<div class='direct-chat-msg'><div class='direct-chat-info clearfix'><span class='direct-chat-name pull-left'>Me</span><span class='direct-chat-timestamp pull-right'>" + date + "</span></div><img class='direct-chat-img' src='http://www.freeiconspng.com/uploads/gym-icon-png-kansas-city-5.png' alt='message user image'><div class='direct-chat-text'>" + message + "</div></div>"));
};