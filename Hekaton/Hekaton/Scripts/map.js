﻿var map;
var infowindow;
var myLocation = { lat: 51.106221899999994, lng: 16.9714854 };
(function() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }

})();
function showPosition(position) {
    myLocation.lat = position.coords.latitude;
    myLocation.lng = position.coords.longitude;
}
function initMap() {

map = new google.maps.Map(document.getElementById('map'), {
    center: myLocation,
    zoom: 15
});

infowindow = new google.maps.InfoWindow();
var service = new google.maps.places.PlacesService(map);
service.nearbySearch({
    location: window.myLocation,
    radius: 10000,
    type: ['gym']
}, callback);
}

function callback(results, status) {
    if (status === google.maps.places.PlacesServiceStatus.OK) {
        createMyMarker();
        for (var i = 0; i < results.length; i++) {
            createMarker(results[i]);
        }
    }
}

function createMarker(place) {
    var placeLoc = place.geometry.location;
    var marker = new google.maps.Marker({
        map: map,
        position: place.geometry.location
    });

    google.maps.event.addListener(marker, 'click', function () {
        infowindow.setContent(place.name);
        infowindow.open(map, this);
    });
}

function createMyMarker() {
    var myLatLng = { lat: 51.106221899999994, lng: 16.9714854 };
    var marker = new google.maps.Marker({
        map: map,
        position: myLatLng
    });
    marker.setIcon('http://maps.google.com/mapfiles/ms/icons/green-dot.png');

    google.maps.event.addListener(marker, 'click', function () {
        infowindow.setContent("pgs");
        infowindow.open(map, this);
    });
}