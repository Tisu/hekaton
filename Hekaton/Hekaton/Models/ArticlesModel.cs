﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hekaton.Models
{
    public class ArticlesModel
    {
        public List<SingleArticleModel> ArticlesList { get; set; } = new List<SingleArticleModel>();
    }
}