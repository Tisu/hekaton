﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Hekaton.Models
{
    public class DailyCaloriesUserModel
    {
        [Required(ErrorMessage = "Field is Required")]
        [Range(0, int.MaxValue, ErrorMessage = "Fields must be a positive number")]
        public int EatenCalories { get; set; }
    }
}