﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Hekaton.Models
{
    public class UserInformationModel
    {
        [Required(ErrorMessage = "Field is Required")]
        [Range(1, 150, ErrorMessage = "Fields must be a positive number")]
        public int Age { get; set; }

        [Required(ErrorMessage = "Field is Required")]
        [Range(1, int.MaxValue, ErrorMessage = "Fields must be a positive number")]
        public int Height { get; set; }

        [Required(ErrorMessage = "Field is Required")]
        [Range(1, int.MaxValue, ErrorMessage = "Fields must be a positive number")]
        public int Weight { get; set; }

        [Required(ErrorMessage = "Field is Required")]
        [Range(0, int.MaxValue, ErrorMessage = "Fields must be a positive number")]
        public int Gender { get; set; }

        [Required(ErrorMessage = "Field is Required")]
        [Range(0, int.MaxValue, ErrorMessage = "Fields must be a positive number")]
        public int BodyType { get; set; }

        [Required(ErrorMessage = "Field is Required")]
        [Range(0, int.MaxValue, ErrorMessage = "Fields must be a positive number")]
        public int Target { get; set; }

        [Required(ErrorMessage = "Field is Required")]
        public int Lifestyle { get; set; }
    }
}