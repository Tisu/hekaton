﻿using System;
using System.Data.SqlTypes;

namespace Hekaton.Models.Calories
{
    public class CaloriesPointModel
    {
        public string Date { get;set; }
        public int Calories { get; set; }
        public int LastMonthCalories { get; set; }
    }
}