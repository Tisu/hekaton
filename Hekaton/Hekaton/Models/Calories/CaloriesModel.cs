﻿using System.Collections.Generic;

namespace Hekaton.Models.Calories
{
    public class CaloriesModel
    {
        public string Email { get; set; }
        public List<CaloriesPointModel> CaloriesPointModels { get; set; } = new List<CaloriesPointModel>();
    }
}