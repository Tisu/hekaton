﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hekaton.Models
{
    public class SingleArticleModel
    {
        public string Name { get; set; }
        public string ArticleUrl { get; set; }

    }
}