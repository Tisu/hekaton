﻿using System;

namespace Hekaton.Models.Planning
{
    public class PlanningData
    {
        public string title { get; set; }
        public string start { get; set; }
    }
}