﻿using System;
using System.Collections.Concurrent;

namespace Hekaton.Models.Planning
{
    public class PlanningCache
    {
        public ConcurrentBag<PlanningData> Cache = new ConcurrentBag<PlanningData>();
    }
}