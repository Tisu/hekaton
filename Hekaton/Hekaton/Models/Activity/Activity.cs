﻿namespace Hekaton.Models.Activity
{
    public class Activity
    {
        public double GetIntensity(WorkoutModel workoutModel)
        {
            return workoutModel.Sets * workoutModel.Reps * workoutModel.Weight;
        }
    }
}