﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Hekaton.Models.Activity
{
    public class WorkoutModel
    {
        [Required(ErrorMessage = "Field is Required")]
        [Range(0, int.MaxValue, ErrorMessage = "Fields must be a positive number")]
        public int Sets { get; set; }
        [Required(ErrorMessage = "Field is Required")]
        [Range(0, int.MaxValue, ErrorMessage = "Fields must be a positive number")]
        public int Reps { get; set; }
        [Required(ErrorMessage = "Field is Required")]
        [Range(0, int.MaxValue, ErrorMessage = "Fields must be a positive number")]
        public double Weight { get; set; }
        [Required(ErrorMessage = "Field is Required")]
        public string DateTime { get; set; }
    }
}