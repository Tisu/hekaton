﻿using System.Collections.Generic;

namespace Hekaton.Models.Activity
{
    public class GraphWorkoutModel
    {
        public string Email { get; set; }
        public List<GraphPointModel> WorkoutModelsList { get; set; } = new List<GraphPointModel>();
    }
}