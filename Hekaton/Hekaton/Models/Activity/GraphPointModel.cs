﻿using System;

namespace Hekaton.Models.Activity
{
    public class GraphPointModel
    {
        public string Date { get; set; }
        public double Intensivity { get; set; }
        public double LastMonthIntensivity { get; set; }
    }
}