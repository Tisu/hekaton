﻿namespace Hekaton.Models.ChatCache
{
    public class ChatMessage
    {
        public string User { get; set; }
        public string Message { get; set; }
    }
}