﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using Hekaton.Formula;
using Hekaton.Hubs;
using Hekaton.Models;
using Hekaton.Models.Activity;
using Hekaton.Models.Calories;
using Hekaton.Repositories.ActivityRepository;
using Hekaton.Services;

namespace Hekaton.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View("NotLogged");
            }

            var  calories = new Calories();
            var data = AppRepo.GetCacheUser(HttpContext.User.Identity.Name);
            var bmi = Math.Round(calories.CalculateBMI(data.UserInformationModelRepo), 1);
            var userBmiInfo = calories.interpreteBMI(bmi);
            data.UserBmiModel.Bmi = bmi;
            data.UserBmiModel.UserBmiInfo = userBmiInfo;

            return View(data.UserBmiModel);
        }

        [Authorize]
        [HttpGet]
        public ActionResult LogActivity()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult LogActivity(WorkoutModel model)
        {
            var activity = new Activity();
            var graphPointModel = new GraphPointModel();

            var result = activity.GetIntensity(model);
            graphPointModel.Date = model.DateTime;
            graphPointModel.Intensivity = result;

            var email = User.Identity.Name;
            var data = AppRepo.GetCacheUser(HttpContext.User.Identity.Name);

            data.graphWorkoutModelRepo.Email = email;
            data.graphWorkoutModelRepo.WorkoutModelsList.Add(
                graphPointModel);

            return RedirectToAction("Index");
        }
        [Authorize]
        [HttpGet]
        public ActionResult CalculatorCalories()
        {
            List<SelectListItem> items = new List<SelectListItem>();

            items.Add(new SelectListItem { Text = "lack of activity", Value = "1" });
            items.Add(new SelectListItem { Text = "sitting work", Value = "2" });
            items.Add(new SelectListItem { Text = "Work not physical", Value = "3", Selected = true });
            items.Add(new SelectListItem { Text = "light work physical", Value = "4" });
            items.Add(new SelectListItem { Text = "work physical", Value = "5" });
            items.Add(new SelectListItem { Text = "hard work physical", Value = "6" });

            ViewData["Lifestyle"] = items;

            return View();
        }
        [Authorize]
        [HttpPost]
        public ActionResult CalculatorCalories(UserInformationModel model)
        {
            var data = AppRepo.GetCacheUser(HttpContext.User.Identity.Name);

            data.UserInformationModelRepo = model;
            return RedirectToAction("CalculatorCalories");
        }

        [Authorize]
        [HttpGet]
        public ActionResult GetArticle()
        {
            var articlesService = new ArticlesService();
            var result = articlesService.GetArticles();
            return View(result);
        }

        [Authorize]
        [HttpGet]
        public ActionResult CaloriesPerDay()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult CaloriesPerDay(CaloriesPointModel model)
        {
            var calories = new Calories();

            var data = AppRepo.GetCacheUser(HttpContext.User.Identity.Name);

            var suggestedcalories = Convert.ToInt32(calories.Calculate(data.UserInformationModelRepo));
            var eatencalories = model.Calories;

            var tmp = data.CaloriesPointModel.CaloriesPointModels
                 .Where(x => x.Date == DateTime.Today.ToString(GraphsHub.date_pattern)).ToList();

            if (tmp.Count == 0)
            {
                model.Calories = suggestedcalories - eatencalories;
                data.CaloriesPointModel.CaloriesPointModels.Add(model);
            }
            else
            {
                tmp.First().Calories += suggestedcalories - eatencalories;
            }
            return RedirectToAction("Index", "Graphs");
        }

        [Authorize]
        [HttpGet]
        public ActionResult GetNearestGym()
        {
            return View();
        }
    }
}