﻿using System.Collections.Generic;
using Hekaton.Models.Planning;

namespace Hekaton.Hubs
{
    public interface IWorkoutPlanningHandler
    {
        void GetSavedData(IEnumerable<PlanningData> data);
    }
}