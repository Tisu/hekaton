﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Hekaton.Models.ChatCache;
using Hekaton.Repositories.ActivityRepository;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace Hekaton.Hubs
{
    [HubName("chat")]
    public class ChatHub : Hub<IChatHandler>
    {
        public void SendMessage(string message)
        {
            var data = AppRepo.GetCacheUser(HttpContext.Current.User.Identity.Name);

            data.ChatCache.Messages.Add(new ChatMessage() { Message = message, User = HttpContext.Current.User.Identity.Name });
            Clients.Others.SendToClient(message, DateTime.Now.ToString("HH:mm"));
        }

        public void OnChatInitialize()
        {
            var data = AppRepo.GetCacheUser(HttpContext.Current.User.Identity.Name);
            Clients.Caller.InitChat(data.ChatCache.Messages, HttpContext.Current.User.Identity.Name);
        }
    }
}