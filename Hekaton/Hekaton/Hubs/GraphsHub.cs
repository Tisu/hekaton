﻿using System;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Security;
using Hekaton.Repositories.ActivityRepository;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace Hekaton.Hubs
{
    [HubName("graphs")]
    public class GraphsHub : Hub<IGraphsHandler>
    {
        public const string date_pattern = "MM/dd/yyyy";
        public void Send()
        {
            var data = AppRepo.GetCacheUser(HttpContext.Current.User.Identity.Name);
            var data_last_month = data.graphWorkoutModelRepo.WorkoutModelsList.Where(x =>
            {
                DateTime parsed;
                if (!DateTime.TryParseExact(x.Date, date_pattern, CultureInfo.InvariantCulture, DateTimeStyles.None, out parsed))
                {
                    return false;
                }
                return parsed > DateTime.Now.AddMonths(-1);
            }).ToList();

            foreach (var single_data in data_last_month)
            {
                
                var one_month_before = data.graphWorkoutModelRepo.WorkoutModelsList.Where(x =>
                    {
                        
                        DateTime parsed;
                        if (!DateTime.TryParseExact(single_data.Date, date_pattern, CultureInfo.InvariantCulture, DateTimeStyles.None, out parsed))
                        {
                            return false;
                        }
                        DateTime parsed1;
                        if (!DateTime.TryParseExact(x.Date, date_pattern, CultureInfo.InvariantCulture, DateTimeStyles.None, out parsed1))
                        {
                            return false;
                        }

                        return parsed.AddMonths(-1) == parsed1;
                    }
                ).FirstOrDefault();
                if (one_month_before == null)
                {
                    continue;
                }
                single_data.LastMonthIntensivity = one_month_before.Intensivity;
            }
            Clients.Caller.SendData(data_last_month);
            Clients.Caller.SendCaloriesData(data.CaloriesPointModel.CaloriesPointModels);
        }
    }
}