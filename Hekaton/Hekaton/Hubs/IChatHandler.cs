﻿using System.Collections.Generic;
using Hekaton.Models.ChatCache;

namespace Hekaton.Hubs
{
    public interface IChatHandler
    {
        void SendToClient(string message,string date);
        void InitChat(List<ChatMessage> ChatMessages,string userName);
    }
}