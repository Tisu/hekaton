﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hekaton.Models.Activity;
using Hekaton.Models.Calories;

namespace Hekaton.Hubs
{
    public interface IGraphsHandler
    {
        void SendData(List<GraphPointModel> graphPointModelList);
        void SendCaloriesData(List<CaloriesPointModel> CalloriesModel);
    }
}
