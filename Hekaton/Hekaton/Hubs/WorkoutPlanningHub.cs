﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Hekaton.Models.Planning;
using Hekaton.Repositories.ActivityRepository;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace Hekaton.Hubs
{
    [HubName("workoutPlanning")]
    public class WorkoutPlanningHub : Hub<IWorkoutPlanningHandler>
    {
        public void SaveData(PlanningData data)
        {
            var userData = AppRepo.GetCacheUser(HttpContext.Current.User.Identity.Name);
            userData.PlanningCache.Cache.Add(data);
        }

        public void GetSavedData()
        {
            var data = AppRepo.GetCacheUser(HttpContext.Current.User.Identity.Name);
            Clients.Caller.GetSavedData(data.PlanningCache.Cache);
        }
    }
}