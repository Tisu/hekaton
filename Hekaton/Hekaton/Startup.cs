﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Hekaton.Startup))]
namespace Hekaton
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
