﻿using System.Collections.Concurrent;

namespace Hekaton.Repositories.ActivityRepository
{
    public class AppRepo
    {
        public const string DATETIME_PATTERN = @"yyyy/MM/dd";
        //hacks in GTD mode , database is too costly
        private static ConcurrentDictionary<string,CachePerUser> Cache = new ConcurrentDictionary<string, CachePerUser>();

        public static CachePerUser GetCacheUser(string userIdentity)
        {
            CachePerUser data;
            if (Cache.TryGetValue(userIdentity, out data))
            {
                return data;
            }
            data = new CachePerUser();
            Cache.TryAdd(userIdentity, data);
            return data;
        }
    }
}