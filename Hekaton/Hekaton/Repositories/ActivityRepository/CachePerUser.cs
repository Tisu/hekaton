﻿using Hekaton.Models;
using Hekaton.Models.Activity;
using Hekaton.Models.Calories;
using Hekaton.Models.ChatCache;
using Hekaton.Models.Planning;

namespace Hekaton.Repositories.ActivityRepository
{
    public class CachePerUser
    {
        public GraphWorkoutModel graphWorkoutModelRepo = new GraphWorkoutModel();
        public UserInformationModel UserInformationModelRepo = new UserInformationModel();
        public UserBmiModel UserBmiModel = new UserBmiModel();
        public ArticlesModel ArticlesModel = new ArticlesModel();
        public CaloriesModel CaloriesPointModel = new CaloriesModel();
        public PlanningCache PlanningCache = new PlanningCache();
        public ChatCache ChatCache = new ChatCache();
    }
}